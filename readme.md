# AjaxLinkPager #

AjaxLinkPager adding ajax functionality to default CLinkPager.

# Using example: #

```php
$this->widget('application.widgets.AjaxLinkPager',array(
	'pages' => $pages, // instance of CPagination class
	// AjaxLinkPager options here:
	'ajaxOptions' => array(
		'dataType' => 'html',
		'success' => 'js: function(data) {
			$("#searchContent").html(data);
		}',
	)
));
```
