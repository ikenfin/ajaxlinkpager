<?php
	/**
	* AjaxLinkPager class file.
	*
	* @author ikenfin <ikenfin@gmail.com>
	* 
	*/

	/**
	* AjaxLinkPager adding ajax functionality to default CLinkPager
	*/
	class AjaxLinkPager extends CLinkPager {

		/**
		* @var array of $.ajax options
		*/
		public $ajaxOptions = array();
		/**
		* @var string the CSS class for ajax page button
		*/
		public $pageSelector = 'ajaxLinkPagerPageSelector';

		/**
		* Initializes the pager by calling parent init, and attaching javascript.
		*/
		public function init() {
			parent::init();

			$this->createJS();
		}

		/**
		* Creates a page button.
		* You may override this method to customize the page buttons.
		* @param string $label the text label for the button
		* @param integer $page the page number
		* @param string $class the CSS class for the page button.
		* @param boolean $hidden whether this page button is visible
		* @param boolean $selected whether this page button is selected
		* @return string the generated button
		*/
		protected function createPageButton($label,$page,$class,$hidden,$selected) {

			if($hidden || $selected)
			$class.= ' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);

			$url = $this->createPageUrl($page);

			//if user switched js off then we use href parameter
			return '<li class="'.$class.'">'.CHtml::link($label,$url,array('href'=>$url,'class'=>$this->pageSelector)).'</li>';
		}


		/**
		* Attaching needed javascript for ajax functionality.
		*/
		private function createJS() {
			$ajaxOptions = $this->ajaxOptions;
			$ajaxOptions['url'] = 'js:this.href';

			if(isset($ajaxOptions['success']) && !(strpos($ajaxOptions['success'], 'js:',0) === 0))
			$ajaxOptions['success'] = 'js:'.$ajaxOptions['success'];

			$ajaxOptions = CJavaScript::encode($ajaxOptions);

			// simple ajax query, based on jquery.ajax
			$js = ";(function($) {
				'use strict';
				$(document.body).on('click','a.{$this->pageSelector}',function(event) {
					event.preventDefault();

					$.ajax(
						{$ajaxOptions}
					);
				});
			}(jQuery));";

			// attach script to page
			Yii::app()->clientScript->registerScript(__CLASS__,$js,CClientScript::POS_LOAD);
		}
	}
